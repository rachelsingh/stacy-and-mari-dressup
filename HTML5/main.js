main_state = {
	name : "state",
	
	Setup: function( canvasWindow, settings ) {
		this.nextState = "";
		this.keys = { up: 38, down: 40, left: 37, right: 39, space: 32 };
		this.drawChanges = true;
		
		this.mari_x = -100;
		this.mari_y = -30;
		this.stacy_x = 400;
		this.stacy_y = -30;
		
		this.background = new Image();
		this.background.src = "graphics/background.png";
		
		this.mari = new Image();
		this.mari_hair = new Image();
		this.mari_top = new Image();
		this.mari_bottom = new Image();
		this.mari_shoes = new Image();
		
		this.mari.src			= "graphics/mari_base.png";		
		this.mari_hair.src 		= "graphics/hair/hairA_colorA_mari.png";
		this.mari_shoes.src 	= "graphics/shoes/shoesA_colorA_mari.png";
		this.mari_top.src 		= "graphics/tops/topsA_colorA_mari.png";
		this.mari_bottom.src 	= "graphics/bottoms/bottomsA_colorA_mari.png";
		
		this.stacy = new Image();
		this.stacy_hair = new Image();
		this.stacy_top = new Image();
		this.stacy_bottom = new Image();
		this.stacy_shoes = new Image();
		
		this.stacy.src 			= "graphics/stacy_base.png";		
		this.stacy_hair.src 	= "graphics/hair/hairB_colorA_stacy.png";
		this.stacy_shoes.src 	= "graphics/shoes/shoesA_colorA_stacy.png";
		this.stacy_top.src 		= "graphics/tops/topsA_colorA_stacy.png";
		this.stacy_bottom.src 	= "graphics/bottoms/bottomsA_colorA_stacy.png";
	},
	
	Name: function() { 
		return this.name; 
	},
	
	HandleKeyDown: function( ev ) {
	},
	
	HandleKeyUp: function( ev ) {
	},
	
	HandleMouseDown: function( ev ) {
	},
	
	Update: function( settings ) {
		return this.nextState;
	},
	
	Draw: function( canvasWindow, settings ) { 
		canvasWindow.fillStyle = "#c96bf0";
		canvasWindow.fillRect( 0, 0, settings.width, settings.height );
		
		canvasWindow.drawImage( this.background, 0, 0 );
		
		canvasWindow.drawImage( this.mari, 			this.mari_x, this.mari_y );
		canvasWindow.drawImage( this.mari_hair, 	this.mari_x, this.mari_y );
		canvasWindow.drawImage( this.mari_shoes, 	this.mari_x, this.mari_y );
		canvasWindow.drawImage( this.mari_bottom, 	this.mari_x, this.mari_y );
		canvasWindow.drawImage( this.mari_top, 		this.mari_x, this.mari_y );
		
		canvasWindow.drawImage( this.stacy, 		this.stacy_x, this.stacy_y );
		canvasWindow.drawImage( this.stacy_hair, 	this.stacy_x, this.stacy_y );
		canvasWindow.drawImage( this.stacy_shoes, 	this.stacy_x, this.stacy_y );
		canvasWindow.drawImage( this.stacy_bottom, 	this.stacy_x, this.stacy_y );
		canvasWindow.drawImage( this.stacy_top, 	this.stacy_x, this.stacy_y );
	},
	
	Redraw: function() {
		if ( this.drawChanges ) {
			this.drawChanges = false;
			return true;
		}
	}
};
