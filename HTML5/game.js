$( document ).ready( function() 
{
	var settings = { width: 800, height: 900, fps: 30 };
	
	$( "canvas" ).attr( "width", settings.width );
	$( "canvas" ).attr( "height", settings.height );
	
	var canvasWindow = $( "canvas" )[0].getContext( "2d" );
	
	// Loading Screen
	canvasWindow.fillStyle = "#398eed";
	canvasWindow.fillRect( 0, 0, settings.width, settings.height );
	canvasWindow.fillStyle = "#ffffff";
	canvasWindow.fillText( "Loading...", 10, 10 );

	setTimeout( FinishSetup, 100 );
    
    main_state.Setup( canvasWindow, settings );

	function FinishSetup() 
	{
        window.addEventListener( "keydown", 	main_state.HandleKeyDown, 	false );
        window.addEventListener( "keyup", 		main_state.HandleKeyUp, 		false );
        window.addEventListener( "mousedown", 	main_state.HandleMouseDown, 	false );
        
        setInterval( function() {
            main_state.Update( settings );
            main_state.Draw( canvasWindow, settings );
        }, 1000 / settings.fps );
	}
    
    
    // Callbacks
    $( ".change_mari_hair" ).click( function() {
        var hairImage = $( this ).attr( "src" );
        main_state.mari_hair.src = hairImage;
    } );
    
    $( ".change_mari_bottoms" ).click( function() {
        var bottomImage = $( this ).attr( "src" );
        main_state.mari_bottom.src = bottomImage;
    } );
    
    $( ".change_mari_tops" ).click( function() {
        var topImage = $( this ).attr( "src" );
        main_state.mari_top.src = topImage;
    } );
    
    $( ".change_mari_shoes" ).click( function() {
        var shoeImage = $( this ).attr( "src" );
        main_state.mari_shoes.src = shoeImage;
    } );
    
    
    
    $( ".change_stacy_hair" ).click( function() {
        var hairImage = $( this ).attr( "src" );
        main_state.stacy_hair.src = hairImage;
    } );
    
    $( ".change_stacy_bottoms" ).click( function() {
        var bottomImage = $( this ).attr( "src" );
        main_state.stacy_bottom.src = bottomImage;
    } );
    
    $( ".change_stacy_tops" ).click( function() {
        var topImage = $( this ).attr( "src" );
        main_state.stacy_top.src = topImage;
    } );
    
    $( ".change_stacy_shoes" ).click( function() {
        var shoeImage = $( this ).attr( "src" );
        main_state.stacy_shoes.src = shoeImage;
    } );
    
    
    
    $( "#mari_hair" ).click( function() {     
        ToggleSection( "mari", "hair" );   
    } );
    
    $( "#mari_tops" ).click( function() {     
        ToggleSection( "mari", "tops" );   
    } );
    
    $( "#mari_bottoms" ).click( function() {     
        ToggleSection( "mari", "bottoms" );   
    } );
    
    $( "#mari_shoes" ).click( function() {     
        ToggleSection( "mari", "shoes" );   
    } );
    
    
    $( "#stacy_hair" ).click( function() {     
        ToggleSection( "stacy", "hair" );   
    } );
    
    $( "#stacy_tops" ).click( function() {     
        ToggleSection( "stacy", "tops" );   
    } );
    
    $( "#stacy_bottoms" ).click( function() {     
        ToggleSection( "stacy", "bottoms" );   
    } );
    
    $( "#stacy_shoes" ).click( function() {     
        ToggleSection( "stacy", "shoes" );   
    } );
    
    function ToggleSection( person, name ) {
        var currentDisplay = $( "." + person + "_" + name + "_container" ).css( "display" );
        
        $( "." + person + "_hair_container" ).fadeOut( "fast", function() { ; } );        
        $( "." + person + "_tops_container" ).fadeOut( "fast", function() { ; } );        
        $( "." + person + "_bottoms_container" ).fadeOut( "fast", function() { ; } );        
        $( "." + person + "_shoes_container" ).fadeOut( "fast", function() { ; } );
        
        if ( currentDisplay == "none" )
        {
            $( "." + person + "_" + name + "_container" ).fadeIn( "fast", function() { ; } ); 
        }               
    }
} 
);
